// vite.config.js
import { resolve } from "path";
import { defineConfig } from "vite";
import { existsSync } from "fs";

export default defineConfig({
	base: "./",
	build: {
		/* minify: true, */
		/* outDir: ".", */
		lib: {
			entry: resolve("src/index.js"),
			formats: ["es"],
			name: "index",
			fileName: "index",
		},
		rollupOptions: {
			output: {
				dir: "dist-lib",
			},
		},
	},
});
