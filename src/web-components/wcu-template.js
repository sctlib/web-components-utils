export function getTemplate(name) {
	return document.querySelector(`template[id="${name}"]`);
}

export default class WCUTemplate extends HTMLElement {
	get $template() {
		return getTemplate(this.template);
	}
	connectedCallback() {
		this.template = this.getAttribute("template");
		if (!this.template) return;
		if (!this.$template) return;
		this._render();
	}
	_render() {
		this.innerHTML = "";
		const $dom = this.$template.content.cloneNode("true");
		this.parentNode.replaceChild($dom, this);
	}
}
