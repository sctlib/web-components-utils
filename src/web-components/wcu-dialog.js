const template = document.createElement("template");
template.innerHTML = `
	<slot name="open">
		<button part="open"></button>
	</slot>
	<dialog part="dialog">
		<slot name="close">
			<form method="dialog" part="form">
				<button part="close"></button>
			</form>
		</slot>
		<slot name="dialog"></slot>
	</dialog>
`;

/* a <dialog/> with a close button and close area around */
export default class WCUDialog extends HTMLElement {
	static get observedAttributes() {
		return ["text-open", "text-close"];
	}
	get textOpen() {
		return this.getAttribute("text-open") || "Open";
	}
	get textClose() {
		return this.getAttribute("text-close") || "Close";
	}
	attributeChangedCallback() {
		this._render();
	}
	constructor() {
		super();
		this.$root = this.attachShadow({ mode: "open" });
		this.$root.appendChild(template.content.cloneNode(true));
	}
	_addEventListeners() {
		this.$dialog.addEventListener("click", this._onDialogClick.bind(this));
		this.$dialog.addEventListener("close", this._onDialogClose.bind(this));
		this.$btnOpen.addEventListener("click", this._onDialogOpen.bind(this));
	}
	_removeEventListeners() {
		this.$dialog.removeEventListener("click", this._onDialogClick.bind(this));
		this.$dialog.removeEventListener("close", this._onDialogClose.bind(this));
		this.$btnOpen.removeEventListener("click", this._onDialogOpen.bind(this));
	}
	connectedCallback() {
		this.$dialog = this.$root.querySelector("dialog");
		this.$form = this.$root.querySelector("form");
		this.$btnOpen = this.$root.querySelector("button[part='open']");
		this.$btnClose = this.$root.querySelector("button[part='close']");
		this.$slot = this.$root.querySelector("slot");

		this._addEventListeners();
		this._render();
	}
	disconnectedCallback() {
		this._removeEventListeners();
	}

	/* public methods */
	open() {
		this.$dialog.showModal();
		const openEvent = new CustomEvent("open", {
			bubbles: false,
			detail: {
				open: this.$dialog.open,
			},
		});
		this.dispatchEvent(openEvent);
	}
	close() {
		const closeEvent = new CustomEvent("close", {
			bubbles: false,
			detail: {
				open: this.$dialog.open,
			},
		});
		this.dispatchEvent(closeEvent);
	}

	/* private methods */
	_render() {
		// $btnClose is default HTML form behavior
		if (this.$btnOpen) {
			this.$btnOpen.innerText = this.textOpen;
		}
		if (this.$btnClose) {
			this.$btnClose.innerText = this.textClose;
		}
	}

	/* https://stackoverflow.com/questions/25864259/how-to-close-the-new-html-dialog-tag-by-clicking-on-its-backdrop */
	_onDialogClick(event) {
		if (!this.$dialog) return;
		const rect = this.$dialog.getBoundingClientRect();
		const isInDialog =
			rect.top <= event.clientY &&
			event.clientY <= rect.top + rect.height &&
			rect.left <= event.clientX &&
			event.clientX <= rect.left + rect.width;

		/* out of the dialog box*/
		if (!isInDialog) {
			this.$dialog.close();
		}
	}
	_onDialogOpen() {
		if (!this.$dialog) return;
		this.open();
	}
	_onDialogClose() {
		this.close();
	}
}
