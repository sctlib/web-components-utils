export default class WCUComponents extends HTMLElement {
	static get observedAttributes() {
		return ["components", "href"];
	}
	get href() {
		return this.getAttribute("href") || "./";
	}
	get components() {
		return JSON.parse(this.getAttribute("components"));
	}
	connectedCallback() {
		this._render();
	}
	attributeChangedCallback() {
		this._render();
	}
	_render() {
		this.innerHTML = "";
		if (!this.components) return;
		const $menu = document.createElement("menu");
		console.log("this.components", this.components);
		const $links = this.components.map((name) => {
			const $li = document.createElement("li");
			const $link = document.createElement("a");
			$link.innerText = name;
			$link.href = `${this.href}${name}/`;
			$li.append($link);
			return $li;
		});
		$menu.append(...$links);
		this.append($menu);
	}
}
