import { isBrowser, isNode } from "./utils/env.js";

import WCUComponents from "./web-components/wcu-components.js";
import WCUDialog from "./web-components/wcu-dialog.js";
import WCUTemplate from "./web-components/wcu-template.js";

/* all the exported components */
const componentDefinitions = {
	"wcu-components": WCUComponents,
	"wcu-template": WCUTemplate,
	"wcu-dialog": WCUDialog,
};

/* auto define all components, if in browser */
export function defineComponents(components = componentDefinitions) {
	if (!isBrowser) return;
	Object.entries(components).map(([cTag, cDef]) => {
		if (!customElements.get(cTag)) {
			customElements.define(cTag, cDef);
		}
	});
}
defineComponents();

export { isBrowser, isNode };
export { componentDefinitions as default };
