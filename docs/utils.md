# `isNode` and `isBrowser` booleans
To determine in which environment our code is interpretated;

# `defineComponents` function
Expects a `componentDefinitions` object with key and value being the name of the HTML tag as used in the dim, and the HTMLElement web-component's Javascript class.
```js
const componentDefinitions = {
	"wcu-components": WCUComponents,
	"wcu-template": WCUTemplate,
	"wcu-dialog": WCUDialog,
}
defineComponents(componentDefinitions)
```
As a result, if not defined, the web-components are registered in the browser.

> We can use `window.customElements.get(<tag-name>)` to check if an
> element is defined.
