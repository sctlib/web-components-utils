# Development
In a node ecosystem run `npm run dev` for a local development server.

# Build
The build tool is `vite` used for two outputs with the `npm run build`
command.

## website
Built into `/dist-website` for the examples as a static page.

## library
Built into `/dist-lib` to publish as a npm package.

# Publish
See the `.gitlab-ci.yml` file.
