# wcu-template

Define a tempalte with HTML content inside, and render it where needed.

```html
<!-- define the template, can be inspected in dom, but not visible -->
<template id="my-template">
	<header>Hello</header>
	<main>World</main>
	<footer>Foo</footer>
	>
</template>

<!-- use the tempalte somewhere else in the DOM -->
<wcu-template template="my-template"></wcu-template>
```
