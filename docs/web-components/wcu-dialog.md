# wcu-dialog

Displays an html dialog with a close button

```html
<wcu-dialog text-open="Click me to open" text-close="Close dialog">
	<aside slot="dialog">Inside the dislog</aside>
</wcu-dialog>
```

## Public methods

- `.open()` opens the dialog
- `.close()` closes the dialog

## Public events

- `.addEventlistener('open')`, on open `{ detail: { open: Boolean } }`
- `.addEventlistener(close)`, on close `{ detail: { open: Boolean } }`

### Slots

the slot `name=""` values are

- `open`
- `close`
- `dialog`

> Use an empty slot, to replace a slot's content with nothing
