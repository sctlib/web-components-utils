# wcu-components

Makes a list from all web-components exported from a javascript file of type
module.

## properties

- `href`, where to create the link to
- `components`, should be `JSON.stringify(['my-element', 'other-el'])`

```html
<wcu-components
	href="../examples/"
	components="['my-element', 'other-el']"
></wcu-components>
```
