# `@sctlib/wcu`
A collection of web-components-utilities, things that could be needed
in various projects.

- [documentation](./docs/)
- [examples](./examples/)
- [website](https://sctlib.gitlab.io/wcu/)
- [code](https://gitlab.com/sctlib/wcu)
- npm install --save [@sctlib/wcu](https://www.npmjs.com/package/@sctlib/wcu)
